SPECFILE            = $(shell find -maxdepth 1 -type f -name *.spec)
SPECFILE_NAME       = $(shell awk '$$1 == "Name:"     { print $$2 }' $(SPECFILE) )
SPECFILE_VERSION    = $(shell awk '$$1 == "Version:"  { print $$2 }' $(SPECFILE) )
SPECFILE_RELEASE    = $(shell awk '$$1 == "Release:"  { print $$2 }' $(SPECFILE) )
DIST               ?= $(shell rpm --eval %{dist})

srpm: sources
	rpmbuild --define '_sourcedir $(PWD)' --define "_topdir $(PWD)/build" --define 'dist $(DIST)' -bs $(SPECFILE)

rpm: sources
	rpmbuild --define '_sourcedir $(PWD)' --define "_topdir $(PWD)/build" --define 'dist $(DIST)' -ba $(SPECFILE)

sources:
	python3 setup.py sdist
	mv dist/*.tar.gz .
	rm -rf dist

clean:
	rm -rf build dist MANIFEST *.tar.gz *.egg-info
	find . | grep .pyc | xargs rm -rf
	find . | grep pycache | xargs rm -rf
