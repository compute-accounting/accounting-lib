%{!?python3_sitelib: %global python3_sitelib %(%{__python3} -c "from distutils.sysconfig import get_python_lib; print (get_python_lib())")}

Name: accountinglib
Summary: Common python functions used by the compute-accounting projects
Version: 0.5
Release: 1%{?dist}
License: GPLv3
BuildArch: noarch
Group: Development/Libraries
Source0: %{name}-%{version}.tar.gz
URL: https://gitlab.cern.ch/compute-accounting/accounting-lib
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python3-devel
BuildRequires: python3-setuptools

%description
Common python functions used by the compute-accounting projects

%prep
%setup -q -n %{name}-%{version}

%build
%{__python3} setup.py build

%install
# Main package
## Python module
%{__rm} -rf %{buildroot}
%{__python3} setup.py install --skip-build --root %{buildroot}

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README.md COPYING
%{python3_sitelib}/%{name}/
%{python3_sitelib}/%{name}-%{version}-py?.?.egg-info

%changelog
* Wed Jan 19 2022 Martin Adam <madam@cern.ch> 0.5-1
- Adding verification of root cern to GET request for data from gar
* Wed Jan 19 2022 Martin Adam <madam@cern.ch> 0.5-0
- Adding is_scope_string_match function to ReportPeriod
* Wed Dec 8 2021 Martin Adam <madam@cern.ch> 0.4-0
- Adding ReportPeriod and ReportScope classes
- Adding automated testing
* Mon Aug 23 2021 Martin Adam <madam@cern.ch> 0.3-3
- Adding gar_hostname parameter to FeNameSanitizer's constructor
* Fri Jun 4 2021 Martin Adam <madam@cern.ch> 0.3-2
- switching to rpm-ci
* Fri May 21 2021 Martin Adam <madam@cern.ch> 0.3-1
- adding desanitized cg names cache
- fixing cache renewal for charge_group_cache
* Fri Mar 5 2021 Martin Adam <madam@cern.ch> 0.2-4
- reverting previous change
* Fri Mar 5 2021 Martin Adam <madam@cern.ch> 0.2-3
- add to both batch7 and service-costing7 repos in koji
* Tue Mar 2 2021 Martin Adam <madam@cern.ch> 0.2-2
- adding cache renewal resiliency
* Thu Feb 25 2021 Martin Adam <madam@cern.ch> 0.2-1
- adding lower case CG list as an option
* Wed Feb 24 2021 Martin Adam <madam@cern.ch> 0.2-0
- adding GAR client class ChargeGroupCache
* Sat Oct 17 2020 Martin Adam <madam@cern.ch> 0.1-5
- ':' added to list of chars to be sanitized
- Tuned build process
* Mon Oct 12 2020 Martin Adam <madam@cern.ch> 0.1-1
- Initial RPM release
