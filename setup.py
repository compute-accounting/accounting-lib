from setuptools import setup, find_packages


def readme():
    f = open('README.md')
    return f.read()


setup(
    name='accountinglib',
    version='0.5',
    author='Accounting Team',
    long_description=readme(),
    author_email='compute-accounting-sprint@cern.ch',
    url='https://gitlab.cern.ch/compute-accounting/accounting-lib',
    packages=find_packages(exclude=["test"]),
    include_package_data=True,
    zip_safe=False,
)
