import datetime

from accountinglib.report_period import ReportPeriod
from accountinglib.report_scope import ReportScope


def test_from_str():
    yearly_str = '2021'
    yearly_rep_rep = ReportPeriod.from_str(yearly_str)
    assert yearly_rep_rep.scope == ReportScope.YEARLY
    assert yearly_rep_rep.year == 2021

    yearly_str = '2021/yearly'
    yearly_rep_rep = ReportPeriod.from_str(yearly_str, field_separator='/')
    assert yearly_rep_rep.scope == ReportScope.YEARLY
    assert yearly_rep_rep.year == 2021

    monthly_str = '2021-11'
    monthly_rep_rep = ReportPeriod.from_str(monthly_str)
    assert monthly_rep_rep.scope == ReportScope.MONTHLY
    assert monthly_rep_rep.year == 2021
    assert monthly_rep_rep.month == 11

    monthly_str = '2021-11-monthly'
    monthly_rep_rep = ReportPeriod.from_str(monthly_str)
    assert monthly_rep_rep.scope == ReportScope.MONTHLY
    assert monthly_rep_rep.year == 2021
    assert monthly_rep_rep.month == 11

    daily_str = '2021-11-13'
    daily_rep_rep = ReportPeriod.from_str(daily_str)
    assert daily_rep_rep.scope == ReportScope.DAILY
    assert daily_rep_rep.year == 2021
    assert daily_rep_rep.month == 11
    assert daily_rep_rep.day == 13

    daily_str = '2021,11,13'
    daily_rep_rep = ReportPeriod.from_str(daily_str, field_separator=',')
    assert daily_rep_rep.scope == ReportScope.DAILY
    assert daily_rep_rep.year == 2021
    assert daily_rep_rep.month == 11
    assert daily_rep_rep.day == 13


def test_get_with_offset():
    rep_per = ReportPeriod(year=2021, month=11, day=13, scope=ReportScope.DAILY)
    rep_per = rep_per.get_with_offset(3)
    assert rep_per.day == 13 + 3

    rep_per = ReportPeriod(year=2021, month=11, scope=ReportScope.MONTHLY)
    rep_per = rep_per.get_with_offset(31)
    assert rep_per.month == 12

    rep_per = ReportPeriod(year=2021, scope=ReportScope.YEARLY)
    rep_per = rep_per.get_with_offset(400)
    assert rep_per.year == 2022

    rep_per = ReportPeriod(year=2021, month=11, day=13, scope=ReportScope.DAILY)
    rep_per = rep_per.get_with_offset(365)
    assert rep_per.day == 13
    assert rep_per.month == 11
    assert rep_per.year == 2022


def test_get_previous():
    rep_per = ReportPeriod(year=2021, month=11, day=13, scope=ReportScope.DAILY)
    prev_per = rep_per.get_previous()
    assert prev_per.day == 12
    assert prev_per.month == 11
    assert prev_per.year == 2021
    assert prev_per.scope == ReportScope.DAILY

    rep_per = ReportPeriod(year=2021, month=11, scope=ReportScope.MONTHLY)
    prev_per = rep_per.get_previous()
    assert prev_per.month == 10
    assert prev_per.year == 2021
    assert prev_per.scope == ReportScope.MONTHLY

    rep_per = ReportPeriod(year=2021, scope=ReportScope.YEARLY)
    prev_per = rep_per.get_previous()
    assert prev_per.year == 2020
    assert prev_per.scope == ReportScope.YEARLY


def test_get_next():
    rep_per = ReportPeriod(year=2021, month=11, day=13, scope=ReportScope.DAILY)
    next_per = rep_per.get_next()
    assert next_per.day == 14
    assert next_per.month == 11
    assert next_per.year == 2021
    assert next_per.scope == ReportScope.DAILY

    rep_per = ReportPeriod(year=2021, month=11, scope=ReportScope.MONTHLY)
    next_per = rep_per.get_next()
    assert next_per.month == 12
    assert next_per.year == 2021
    assert next_per.scope == ReportScope.MONTHLY

    rep_per = ReportPeriod(year=2021, month=12, scope=ReportScope.MONTHLY)
    next_per = rep_per.get_next()
    assert next_per.month == 1
    assert next_per.year == 2022
    assert next_per.scope == ReportScope.MONTHLY

    rep_per = ReportPeriod(year=2021, scope=ReportScope.YEARLY)
    next_per = rep_per.get_next()
    assert next_per.year == 2022
    assert next_per.scope == ReportScope.YEARLY


def test_to_str():
    rep_per = ReportPeriod(year=2021, month=11, day=13, scope=ReportScope.DAILY)
    assert str(rep_per) == "2021-11-13"

    rep_per = ReportPeriod(year=2021, month=11, scope=ReportScope.MONTHLY)
    assert str(rep_per) == "2021-11"

    rep_per = ReportPeriod(year=2021, scope=ReportScope.YEARLY)
    assert str(rep_per) == "2021"


def test_to_path():
    rep_per = ReportPeriod(year=2021, month=11, day=13, scope=ReportScope.DAILY)
    assert rep_per.to_path() == "2021/11/13"
    assert rep_per.to_path(target_scope=ReportScope.DAILY) == "2021/11/13"
    assert rep_per.to_path(target_scope=ReportScope.MONTHLY) == "2021/11/monthly"
    assert rep_per.to_path(target_scope=ReportScope.YEARLY) == "2021/yearly"

    rep_per = ReportPeriod(year=2021, month=11, scope=ReportScope.MONTHLY)
    assert rep_per.to_path() == "2021/11/monthly"
    assert rep_per.to_path(
        target_scope=ReportScope.DAILY) == "2021/11/{01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31}"
    assert rep_per.to_path(target_scope=ReportScope.MONTHLY) == "2021/11/monthly"
    assert rep_per.to_path(target_scope=ReportScope.YEARLY) == "2021/yearly"

    rep_per = ReportPeriod(year=2021, scope=ReportScope.YEARLY)
    assert rep_per.to_path() == "2021/yearly"
    assert rep_per.to_path(
        target_scope=ReportScope.DAILY) == "2021/*/{01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31}"
    assert rep_per.to_path(target_scope=ReportScope.MONTHLY) == "2021/*/monthly"
    assert rep_per.to_path(target_scope=ReportScope.YEARLY) == "2021/yearly"


def test_to_datetime():
    rep_per = ReportPeriod(year=2021, month=11, day=13, scope=ReportScope.DAILY)
    rep_date = rep_per.to_datetime()
    assert rep_date.day == rep_per.day
    assert rep_date.month == rep_per.month
    assert rep_date.year == rep_per.year

    rep_per = ReportPeriod(year=2021, month=11, scope=ReportScope.MONTHLY)
    rep_date = rep_per.to_datetime()
    assert rep_date.day == 1
    assert rep_date.month == rep_per.month
    assert rep_date.year == rep_per.year

    rep_per = ReportPeriod(year=2021, scope=ReportScope.YEARLY)
    rep_date = rep_per.to_datetime()
    assert rep_date.day == 1
    assert rep_date.month == 1
    assert rep_date.year == rep_per.year


def test_from_datetime():
    rep_date = datetime.datetime(year=2021, month=11, day=13)
    rep_per = ReportPeriod.from_datetime(date=rep_date, wanted_scope=ReportScope.DAILY)
    assert rep_per.scope == ReportScope.DAILY
    assert rep_date.day == rep_per.day
    assert rep_date.month == rep_per.month
    assert rep_date.year == rep_per.year

    rep_per = ReportPeriod.from_datetime(date=rep_date, wanted_scope=ReportScope.MONTHLY)
    assert rep_per.scope == ReportScope.MONTHLY
    assert rep_date.month == rep_per.month
    assert rep_date.year == rep_per.year

    rep_per = ReportPeriod.from_datetime(date=rep_date, wanted_scope=ReportScope.YEARLY)
    assert rep_per.scope == ReportScope.YEARLY
    assert rep_date.year == rep_per.year


def test_from_timestamp():
    rep_date = datetime.datetime(year=2021, month=11, day=13)
    rep_ts = rep_date.timestamp()
    rep_per = ReportPeriod.from_timestamp(timestamp=rep_ts, wanted_scope=ReportScope.DAILY)
    assert rep_per.scope == ReportScope.DAILY
    assert rep_date.day == rep_per.day
    assert rep_date.month == rep_per.month
    assert rep_date.year == rep_per.year

    rep_per = ReportPeriod.from_timestamp(timestamp=rep_ts, wanted_scope=ReportScope.MONTHLY)
    assert rep_per.scope == ReportScope.MONTHLY
    assert rep_date.month == rep_per.month
    assert rep_date.year == rep_per.year

    rep_per = ReportPeriod.from_timestamp(timestamp=rep_ts, wanted_scope=ReportScope.YEARLY)
    assert rep_per.scope == ReportScope.YEARLY
    assert rep_date.year == rep_per.year


def test_is_more_recent():
    rep_per = ReportPeriod(year=2021, month=11, day=13, scope=ReportScope.DAILY)
    rep_per_older = ReportPeriod(year=2021, month=11, day=11, scope=ReportScope.DAILY)
    rep_per_younger = ReportPeriod(year=2021, month=11, day=15, scope=ReportScope.DAILY)
    assert rep_per.is_more_recent(rep_per_older) is True
    assert rep_per.is_more_recent(rep_per) is False
    assert rep_per.is_more_recent(rep_per_younger) is False

    rep_per = ReportPeriod(year=2021, month=11, scope=ReportScope.MONTHLY)
    rep_per_older = ReportPeriod(year=2021, month=10, scope=ReportScope.MONTHLY)
    assert rep_per.is_more_recent(rep_per_older) is True
    assert rep_per.is_more_recent(rep_per) is False
    assert rep_per_older.is_more_recent(rep_per) is False


def test_get_age_days():
    rep_per = ReportPeriod(year=2021, month=11, day=13, scope=ReportScope.DAILY)
    rep_date_week_ago = datetime.datetime(year=2021, month=11, day=6)
    rep_date_week_in = datetime.datetime(year=2021, month=11, day=19)
    rep_date = datetime.datetime(year=2021, month=11, day=13)

    assert rep_per.get_age_days(rep_date_week_ago) == -7
    assert rep_per.get_age_days(rep_date_week_in) == 6
    assert rep_per.get_age_days(rep_date) == 0


def test_get_scope_str():
    rep_per = ReportPeriod(year=2021, month=11, day=13, scope=ReportScope.DAILY)
    assert rep_per.get_scope_str() == "day"

    rep_per = ReportPeriod(year=2021, month=11, scope=ReportScope.MONTHLY)
    assert rep_per.get_scope_str() == "month"

    rep_per = ReportPeriod(year=2021, scope=ReportScope.YEARLY)
    assert rep_per.get_scope_str() == "year"


def test_is_scope_string_match():
    assert ReportPeriod.is_scope_string_match("2021", "year") is True
    assert ReportPeriod.is_scope_string_match("2021/10", "month") is True
    assert ReportPeriod.is_scope_string_match("2021/10/10", "day") is True

    assert ReportPeriod.is_scope_string_match("2021-10-10", "day", field_separator="-") is True
    assert ReportPeriod.is_scope_string_match("2021-10-10", "day") is False

    assert ReportPeriod.is_scope_string_match("2021/10", "day") is False
    assert ReportPeriod.is_scope_string_match("2021", "day") is False
    assert ReportPeriod.is_scope_string_match("2021/10/10", "month") is False
    assert ReportPeriod.is_scope_string_match("2021", "month") is False
    assert ReportPeriod.is_scope_string_match("2021/10/10", "year") is False
    assert ReportPeriod.is_scope_string_match("2021/10", "year") is False

    assert ReportPeriod.is_scope_string_match("2021/10", "blah") is False
    assert ReportPeriod.is_scope_string_match("blah", "year") is False
