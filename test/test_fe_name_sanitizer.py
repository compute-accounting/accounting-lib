from accountinglib.fe_name_sanitizer import FeNameSanitizer


def test_sanitize_fe_name():
    non_sanitized_fes = ["Acron", "AFS",  # no special characters, just need to keep case
                         "BOINC Support",  # space
                         "Networking for LHC (LHCOPN, LHCONE, IndiaLink, ESnet)",  # spaces, commas and brackets
                         "CERNBox machines - EOS user/project/home/media",  # hyphen and slashes
                         "CERN Users' Office",  # apostrophe
                         "EOSCsecretariat.eu",  # dot
                         "WLCG R&D Coordination"  # &
                         ]

    sanitized_fes = ["Acron", "AFS",  # no special characters, just need to keep case
                         "BOINC_Support",  # space
                         "Networking_for_LHC__LHCOPN__LHCONE__IndiaLink__ESnet_",  # spaces, commas and brackets
                         "CERNBox_machines_-_EOS_user_project_home_media",  # hyphen and slashes
                         "CERN_Users__Office",  # apostrophe
                         "EOSCsecretariat_eu",  # dot
                         "WLCG_R_D_Coordination"  # &
                         ]

    after_sanitization_fes = map(FeNameSanitizer.sanitize_fe_name, non_sanitized_fes)

    assert sanitized_fes == list(after_sanitization_fes)

