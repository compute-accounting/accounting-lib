from enum import Enum


class ReportScope(Enum):
    DAILY = "day"
    MONTHLY = "month"
    YEARLY = "year"

    def __str__(self):
        return self.value
