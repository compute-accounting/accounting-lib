import datetime
from typing import Dict

import accountinglib.utils.constants as consts
from accountinglib.report_scope import ReportScope


class ReportPeriod:
    """
    Helper class with methods concerning the reports time scope
    """
    scope: ReportScope
    day: int
    month: int
    year: int
    FIELD_SEPARATOR: str = '-'
    ALL_DAYS_NUMS = ','.join(map(lambda x: f'{x:02}', range(1, 32)))

    def __init__(self, scope: ReportScope, year: int, month: int = -1, day: int = -1):
        self.scope = scope
        self.day = day
        self.month = month
        self.year = year

    @classmethod
    def from_str(cls, report_str: str, field_separator: str = FIELD_SEPARATOR, raise_error_on_weird_year: bool = False) \
            -> 'ReportPeriod':
        """
        Parse date into ReportPeriod
        :param report_str: string to be parsed
        :param field_separator: char separating years, months and days. Default is '-'
        :param raise_error_on_weird_year: throw a RuntimeError if the year is less than 2010 - can happen if date is
        input wrong
        """
        date_list = report_str.split(field_separator)
        year = int(date_list[0])

        if raise_error_on_weird_year and year < 2010:
            raise RuntimeError(
                f"Parsed year {year} lower than 2010, are you sure date is in format %Y-%m-%d => day, %Y-%m => month, %Y => year?")

        if len(date_list) == 1 or 'yearly' in report_str:
            return ReportPeriod(ReportScope.YEARLY, year, -1, -1)
        elif len(date_list) == 2 or 'monthly' in report_str:
            return ReportPeriod(ReportScope.MONTHLY, year, int(date_list[1]), -1)
        elif len(date_list) >= 3:
            return ReportPeriod(ReportScope.DAILY, year, int(date_list[1]), int(date_list[2]))

    def __str__(self) -> str:
        return self.to_str(self.FIELD_SEPARATOR)

    def get_with_offset(self, to_add_days: int) -> 'ReportPeriod':
        if self.scope == ReportScope.YEARLY:
            date = self.to_datetime()
            date += datetime.timedelta(days=to_add_days)
            return ReportPeriod(ReportScope.YEARLY, year=date.year)
        elif self.scope == ReportScope.MONTHLY:
            date = self.to_datetime()
            date += datetime.timedelta(days=to_add_days)
            return ReportPeriod(ReportScope.MONTHLY, year=date.year, month=date.month)
        elif self.scope == ReportScope.DAILY:
            date = self.to_datetime()
            date += datetime.timedelta(days=to_add_days)
            return ReportPeriod(ReportScope.DAILY, year=date.year, month=date.month, day=date.day)

    def get_previous(self) -> 'ReportPeriod':
        return self.get_with_offset(-1 * consts.PERIOD_CONVERSION_COEF[ReportScope.DAILY][self.scope])

    def get_next(self) -> 'ReportPeriod':
        return self.get_with_offset(consts.PERIOD_CONVERSION_COEF[ReportScope.DAILY][self.scope])

    def to_str(self, field_separator: str = FIELD_SEPARATOR) -> str:
        if self.scope == ReportScope.YEARLY:
            return f"{self.year}"
        elif self.scope == ReportScope.MONTHLY:
            return f"{self.year}{field_separator}{self.month:02}"
        elif self.scope == ReportScope.DAILY:
            return f"{self.year}{field_separator}{self.month:02}{field_separator}{self.day:02}"

    def to_path(self, target_scope: ReportScope = None) -> str:
        """
        Convert to the date part of the S3 "path"
        :param target_scope: If there is a particular scope of data needed to be addressed, use this option. Defaults to self.scope
        :return: str to be inserted into date part of the S3 path representing the report_period
        """
        if target_scope is None:
            target_scope = self.scope

        if target_scope == ReportScope.YEARLY:
            return f"{self.year}/yearly"

        if self.scope == ReportScope.YEARLY:
            if target_scope == ReportScope.MONTHLY:
                return f"{self.year}/*/monthly"
            else:  # DAILY scope
                return f"{self.year}/*/{{{self.ALL_DAYS_NUMS}}}"

        elif self.scope == ReportScope.MONTHLY:
            if target_scope == ReportScope.MONTHLY:
                return f"{self.year}/{self.month:02}/monthly"
            else:  # DAILY scope
                return f"{self.year}/{self.month:02}/{{{self.ALL_DAYS_NUMS}}}"

        else:  # DAILY scope
            if target_scope == ReportScope.MONTHLY:
                return f"{self.year}/{self.month:02}/monthly"
            else:  # DAILY scope
                return f"{self.year}/{self.month:02}/{self.day:02}"

    def to_datetime(self) -> datetime.datetime:
        if self.scope == ReportScope.YEARLY:
            return datetime.datetime(year=self.year, month=1, day=1)
        elif self.scope == ReportScope.MONTHLY:
            return datetime.datetime(year=self.year, month=self.month, day=1)
        elif self.scope == ReportScope.DAILY:
            return datetime.datetime(year=self.year, month=self.month, day=self.day)

    @classmethod
    def from_datetime(cls, date: datetime.datetime, wanted_scope: ReportScope) -> 'ReportPeriod':
        if wanted_scope == ReportScope.YEARLY:
            return ReportPeriod(scope=wanted_scope, year=date.year, month=-1, day=-1)
        elif wanted_scope == ReportScope.MONTHLY:
            return ReportPeriod(scope=wanted_scope, year=date.year, month=date.month, day=-1)
        elif wanted_scope == ReportScope.DAILY:
            return ReportPeriod(scope=wanted_scope, year=date.year, month=date.month, day=date.day)

    @classmethod
    def from_timestamp(cls, timestamp: float, wanted_scope: ReportScope) -> 'ReportPeriod':
        return cls.from_datetime(datetime.datetime.fromtimestamp(timestamp), wanted_scope)

    def is_more_recent(self, other_date: 'ReportPeriod') -> bool:
        return self.to_datetime() > other_date.to_datetime()

    def is_still_viable(self, other_date: 'ReportPeriod', max_age_dict: Dict[ReportScope, int]) -> bool:
        difference = self.to_datetime() - other_date.to_datetime()
        return difference.days <= max_age_dict[other_date.scope]

    def is_relevant(self, other_date: 'ReportPeriod', max_age_dict: Dict[ReportScope, int]) -> bool:
        """
        Check, whether some date is older but withing max_age_days away from current report period.
        Example usage: data written on date_str are still relevant for report done with `this` ReportPeriod
        :param other_date: date in ReportPeriod form of the data to be compared
        :param max_age_dict:
        :return: True if relevant else False
        """
        return self.is_more_recent(other_date) and self.is_still_viable(other_date, max_age_dict)

    def get_age_days(self, age_from: datetime) -> int:
        """
        Returns age from a selected datetime
        :param age_from: datetime from which the delta should be based
        :return: age in days
        """
        delta = age_from - self.to_datetime()
        return delta.days

    def get_scope_str(self) -> str:
        return str(self.scope)

    @classmethod
    def is_scope_string_match(cls, timestamp_string: str, scope_string: str, field_separator: str = '/') -> bool:
        try:
            return cls.from_str(timestamp_string, field_separator=field_separator).scope == ReportScope(scope_string)
        except ValueError:
            return False
