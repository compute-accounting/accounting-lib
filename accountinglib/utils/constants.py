from accountinglib.report_scope import ReportScope

PERIOD_CONVERSION_COEF = {
    ReportScope.YEARLY: {
        ReportScope.YEARLY: 1.0,
        ReportScope.MONTHLY: 1.0 / 12,
        ReportScope.DAILY: 1.0 / 366,
    },
    ReportScope.MONTHLY: {
        ReportScope.YEARLY: 12.0,
        ReportScope.MONTHLY: 1.0,
        ReportScope.DAILY: 1.0 / 31
    },
    ReportScope.DAILY: {
        ReportScope.YEARLY: 366.0,
        ReportScope.MONTHLY: 31.0,
        ReportScope.DAILY: 1.0
    }
}

PERIOD_CONVERSION_COEF_STR = {
    "year": {
        "year": 1.0,
        "month": 1.0 / 12,
        "day": 1.0 / 366,
    },
    "month": {
        "year": 12.0,
        "month": 1.0,
        "day": 1.0 / 31
    },
    "day": {
        "year": 366.0,
        "month": 31.0,
        "day": 1.0
    }
}