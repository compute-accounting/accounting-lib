"""
# Copyright European Organization for Nuclear Research (CERN)
#
# This software is distributed under the terms of the
# GNU General Public Licence version 3 (GPL Version 3), copied verbatim in the
# file COPYING.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.
#
#
"""
import itertools
from datetime import datetime

from accountinglib.charge_group_cache import ChargeGroupCache


class FeNameSanitizer:
    FORBIDDEN_CHARS = ["/", " ", ",", ";", ":", ".", "'", '"', "?", "&", "=", "(", ")"]
    CACHE_AGE_LIMIT_HOURS = 48
    GAR_HOSTNAME = "gar.cern.ch"

    cg_cache = None
    sanitized_names_mapping_cache = {}
    sanitized_names_mapping_cache_timespamp = 0

    def __init__(self, gar_hostname_p=None):
        gar_hostname = gar_hostname_p if gar_hostname_p else self.GAR_HOSTNAME
        self.cg_cache = ChargeGroupCache(gar_hostname=gar_hostname,
                                         cache_max_age_hours=self.CACHE_AGE_LIMIT_HOURS - 1)
        self._update_desanitized_cache()

    @staticmethod
    def sanitize_fe_name(fe_name):
        """
        Sanitize the name of a charge group (sorry for the confusing naming), so that it can be used as a filename etc.

        :param fe_name: string to be sanitized
        :return: fe name compatible with paths and URLs

        """
        res = fe_name
        for char in FeNameSanitizer.FORBIDDEN_CHARS:
            res = res.replace(char, "_")

        return res

    def _update_desanitized_cache(self):
        sanitized_names = map(FeNameSanitizer.sanitize_fe_name, self.cg_cache.cg_list)
        self.sanitized_names_mapping_cache = dict(zip(sanitized_names, self.cg_cache.cg_list))
        self.sanitized_names_mapping_cache_timespamp = datetime.now().timestamp()

    def desanitize_fe_name(self, fe_name_sanitized):
        """
        Try to desanitize a sanitized charge group (sorry for the confusing naming) name. Could be used to try and guess
        the main CG of a S3 file etc.

        :param fe_name_sanitized: string to be desanitized
        :return: original cg name
        :raises: KeyError if fe_name_sanitized is not in the mapping and therefore cannot be resolved
        """
        if not self.cg_cache:
            self.cg_cache = ChargeGroupCache(gar_hostname=self.GAR_HOSTNAME, cache_max_age_hours=self.CACHE_AGE_LIMIT_HOURS-1)
            self._update_desanitized_cache()

        if ChargeGroupCache.is_older_than(self.sanitized_names_mapping_cache_timespamp, self.CACHE_AGE_LIMIT_HOURS):
            self._update_desanitized_cache()

        if fe_name_sanitized not in self.sanitized_names_mapping_cache:
            return fe_name_sanitized

        return self.sanitized_names_mapping_cache[fe_name_sanitized]
