"""
# Copyright European Organization for Nuclear Research (CERN)
#
# This software is distributed under the terms of the
# GNU General Public Licence version 3 (GPL Version 3), copied verbatim in the
# file COPYING.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.
#
#
"""

from datetime import datetime

import requests
import logging

LIST_URL_PATH = "/public/list"
FULL_DICT_URL_PATH = "/public/list_full"


class ChargeGroupCache:
    """
    Client object for the GAR service. Provides a friendly python interface + keeps a cache of the CGs, so that the
    service itself is not overloaded
    """

    cg_list = []
    cg_list_lower = []
    cg_list_timestamp = -1

    cg_full_dict = {}
    cg_full_dict_timestamp = -1

    gar_hostname = ""
    cache_max_age_hours = 0
    
    def __init__(self, gar_hostname, cache_max_age_hours=1):
        self.gar_hostname = gar_hostname
        self.cache_max_age_hours = cache_max_age_hours

        self.cg_full_dict, self.cg_full_dict_timestamp = self._update_cache(FULL_DICT_URL_PATH)
        # Extract names from dict to save one request
        self.cg_list, self.cg_list_timestamp = [cg["name"] for cg in self.cg_full_dict], self.cg_full_dict_timestamp
        self.cg_list_lower = list(map(lambda x: x.lower(), self.cg_list))

    def _update_cache(self, endpoint):
        try:
            response = requests.get(f"https://{self.gar_hostname}{endpoint}",
                                    verify='/etc/pki/tls/certs/CERN-bundle.pem')
        except OSError:
            response = requests.get(f"https://{self.gar_hostname}{endpoint}", verify=False)

        if response.status_code == 200:
            return response.json()["data"], datetime.now().timestamp()
        else:
            raise requests.exceptions.RequestException(f"Request status code not OK, but {str(response.status_code)}")

    @staticmethod
    def is_older_than(timestamp, age_limit):
        return ((datetime.now() - datetime.fromtimestamp(timestamp)).total_seconds()) / 3600 > age_limit

    def get_cg_list(self, lower=False):
        """
        Get list of valid charge groups
        :param lower: boolean indicating, whether the results should be all lowercase
        :return: list of strings - valid charge groups
        """
        if ChargeGroupCache.is_older_than(self.cg_list_timestamp, self.cache_max_age_hours):
            try:
                self.cg_list, self.cg_list_timestamp = self._update_cache(LIST_URL_PATH)
                self.cg_list_lower = list(map(lambda x: x.lower(), self.cg_list))
            except requests.exceptions.RequestException as e:
                logging.error(f"Cannot refresh cache {str(e)}")

        return self.cg_list_lower if lower else self.cg_list

    def get_cg_full_dict(self):
        """
        Get a list of dictionaries with full charge group details
        :return: list of dicts - full charge group details
        """
        if ChargeGroupCache.is_older_than(self.cg_full_dict_timestamp, self.cache_max_age_hours):
            self.cg_full_dict, self.cg_full_dict_timestamp = self._update_cache(FULL_DICT_URL_PATH)

        return self.cg_full_dict
